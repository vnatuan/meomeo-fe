import { Invoice } from 'models/invoice';
import { Payment } from 'models/payment';
import { User } from 'models/user';
import { UserAmount } from 'models/userAmount';
import { ProductModel } from 'models/product';
import { post, postWithBearer, request } from 'utils/request';
import create from 'zustand';
import { hoadon } from 'models/hoadon';
import { CustomerModel } from 'models/customer';

interface AppState {
  bears: number;
  products: Array<ProductModel>;
  danhSachDonHang: Array<hoadon>;
  danhSachAddAmount: Array<hoadon>;
  selectedProduct?: ProductModel;
  customers: Array<CustomerModel>;
  customer?: CustomerModel;
  invoices?: [];
  invoice?: Invoice;
  naptienInvoice?: Invoice;
  user?: User;
  userAmount?: UserAmount;
  payment?: Payment;
  errorMessage?: string;
  modalTitle?: string;
  getProducts: () => void;
  getProduct: (id) => void;
  getPayment: (id) => void;
  searchPayment: (id) => void;
  searchCustomer: (id) => void;
  getDonHangByUserId: (id) => void;
  getAddAmountHistoryByUserId: (id) => void;
  createInvoice: (id, quantity, email, paymentType, isUser) => any;
  createInvoiceTraTruoc: (id, quantity, email, paymentType, isUser) => any;
  addAmount: (email, amount) => any;
  addAmountAdmin: (id, amount) => any;
  registerUser: (name, email, password, password_confirmation) => any;
  loginUser: (email, password) => any;
  getUserAmount: (id, currency_code) => any;
  changePassword: (
    // email,
    password,
    new_password,
    new_password_confirmation,
  ) => any;
  resetPassword: (email) => any;
  uploadEmailsFile: (formdata) => any;
}

function setUserToken(user) {
  sessionStorage.setItem('user', user);
}

function getUserToken() {
  const userString = sessionStorage.getItem('user');
  if (userString != null) {
    let user = JSON.parse(userString);
    const currentTimeStamp = Date.now();
    if (currentTimeStamp >= user.expires_in) {
      sessionStorage.removeItem('user');
    }
    return user.access_token;
  }
  return '';
}

function getTimeSession(sessionTime) {
  const timestamp = Date.now(); // This would be the timestamp you want to format

  const expired_timestamp = timestamp + sessionTime;

  return expired_timestamp;
}

const useStore = create<AppState>()(set => ({
  bears: 0,
  products: [],
  customers: [],
  danhSachDonHang: [],
  danhSachAddAmount: [],
  getProducts: async () => {
    const planetsData = (await request('api/email-types')) as any;
    set({
      products: planetsData,
    });
  },
  getProduct: async id => {
    const planetsData = (await request(`api/email-types/${id}`)) as any;
    set({
      selectedProduct: planetsData[0],
    });
  },
  getUserAmount: async (id, currency_code) => {
    const planetsData = (await request(
      `api/tongtien/${id}/loaitiente/${currency_code}`,
    )) as any;
    console.log(planetsData);
    set({
      userAmount: planetsData.userAmount,
    });
  },

  createInvoice: async (id, quantity, email, paymentType, isUser) => {
    try {
      const planetsData = await post(`api/invoices`, {
        id_email_type: id,
        customer_email: email,
        quantity: quantity,
        payment_type: paymentType ?? 'USDTTRC20',
        is_user: isUser,
      });
      console.log(planetsData[0]);
      const invoice = planetsData[0] as Invoice;

      set({
        invoice,
      });
    } catch (error) {
      handelError(error, set);
    }
  },

  createInvoiceTraTruoc: async (id, quantity, email, paymentType, isUser) => {
    try {
      let token = getUserToken();
      console.log(token);
      await postWithBearer(
        `api/auth/mua-email-tra-truoc`,
        {
          id_email_type: id,
          customer_email: email,
          quantity: quantity,
          payment_type: paymentType ?? 'USDTTRC20',
          is_user: isUser,
        },
        token,
      )
        .then(function (response) {
          const invoice = response[0] as Invoice;
          set({
            invoice,
          });
          console.log(response);
        })
        .catch(function (error) {
          console.log(error);
          handelError(error, set);
        });
    } catch (error) {
      handelError(error, set);
    }
  },

  addAmount: async (email, amount) => {
    try {
      const planetsData = await post(`api/naptien`, {
        user_email: email,
        amount: amount,
      });
      console.log(planetsData);
      const naptienInvoice = planetsData[0] as Invoice;
      set({
        naptienInvoice,
      });
    } catch (error) {
      handelError(error, set);
    }
  },

  addAmountAdmin: async (id, amount) => {
    try {
      await post(`api/admin/update-money`, {
        user_id: id,
        amount: amount,
      })
        .then(function () {
          showMessage('Nạp tiền thành công !', set);
        })
        .catch(function (error) {
          console.log('loi');
          handelError(error, set);
        });
    } catch (error) {
      handelError(error, set);
    }
  },

  uploadEmailsFile: async formdata => {
    try {
      fetch('http://meomeo.io:8000/api/admin/import-emails', {
        method: 'POST',
        body: formdata,
      })
        .then(function () {
          showMessage('Upload file thành công !', set);
        })
        .catch(function (error) {
          console.log('loi');
          handelError(error, set);
        });
    } catch (error) {
      handelError(error, set);
    }
  },

  getPayment: async id => {
    const { payment } = (await request(`api/payments/${id}`)) as any;
    set({
      payment: payment,
    });
  },

  searchPayment: async id => {
    const { payment } = (await request(`api/payments/search/${id}`)) as any;
    console.log(payment);
    set({
      payment: payment,
    });
  },

  searchCustomer: async email => {
    const { customer } = (await request(
      `api/admin/customers-info/${email}`,
    )) as any;
    console.log(customer);
    set({
      customer: customer,
    });
  },

  getDonHangByUserId: async id => {
    const planetsData = (await request(`api/hoadon/user/${id}/type/2`)) as any;
    console.log(planetsData);
    set({
      danhSachDonHang: planetsData.donhang,
    });
  },

  getAddAmountHistoryByUserId: async id => {
    const planetsData = (await request(`api/hoadon/user/${id}/type/3`)) as any;
    set({
      danhSachAddAmount: planetsData.donhang,
    });
  },

  registerUser: async (name, email, password, password_confirmation) => {
    try {
      await post(`api/auth/register`, {
        name: name,
        email: email,
        password: password,
        password_confirmation: password_confirmation,
        role: 2,
      })
        .then(function (registerUser) {
          const invoice = registerUser[0] as Invoice;

          set({
            invoice,
          });

          showMessage('Register Successful !', set);
        })
        .catch(function (error) {
          console.log('loi');
          handelError(error, set);
        });
    } catch (error) {
      handelError(error, set);
    }
  },
  loginUser: async (email, password) => {
    try {
      await post(`api/auth/login`, {
        email: email,
        password: password,
      })
        .then(function (response) {
          console.log(response);
          let loginUser = response as any;
          const user = {
            id: loginUser.user.id,
            access_token: loginUser.access_token,
            expires_in: getTimeSession(3600000),
            token_type: loginUser.token_type,
            role: loginUser.user.role,
            name: loginUser.user.name,
            email: loginUser.user.email,
            created_at: loginUser.user.created_at,
            updated_at: loginUser.user.updated_at,
          };

          setUserToken(JSON.stringify(user));
          set({ user: user });
        })
        .catch(function (error) {
          console.log(error);
          handelError(error, set);
        });
    } catch (error) {
      console.log(error);
      handelError(error, set);
    }
  },

  changePassword: async (
    // email,
    old_password,
    new_password,
    new_password_confirmation,
  ) => {
    try {
      let token = getUserToken();
      console.log(token);
      await postWithBearer(
        `api/auth/change-pass`,
        {
          old_password: old_password,
          new_password: new_password,
          new_password_confirmation: new_password_confirmation,
        },
        token,
      )
        .then(function (response) {
          showMessage('Change Password Successful !', set);
          sessionStorage.removeItem('user');
        })
        .catch(function (error) {
          console.log(error);
          handelError(error, set);
        });
    } catch (error) {
      handelError(error, set);
    }
  },
  resetPassword: async email => {
    try {
      let token = getUserToken();
      console.log(token);
      await postWithBearer(
        `api/auth/reset-password`,
        {
          email: email,
        },
        token,
      )
        .then(function (response) {
          showMessage('Reset Password Successful , Please check email!', set);
        })
        .catch(function (error) {
          console.log(error);
          handelError(error, set);
        });
    } catch (error) {
      handelError(error, set);
    }
  },
}));

function showMessage(message, set) {
  message = message.split('\\n').join(' ');
  set({
    modalTitle: 'SUCCESS',
    errorMessage: message,
  });
  setTimeout(() => {
    set({
      modalTitle: 'SUCCESS',
      errorMessage: undefined,
    });
  }, 200);
}

function handelError(error, set) {
  let message: string = error?.message ?? 'Something error';
  message = message.split('\\n').join(' ');
  set({
    modalTitle: 'ERROR',
    errorMessage: message,
  });
  setTimeout(() => {
    set({
      modalTitle: 'ERROR',
      errorMessage: undefined,
    });
  }, 200);
}

export default useStore;
