export class ResponseError extends Error {
  public response: Response;

  constructor(response: Response) {
    super(response.statusText);
    this.response = response;
  }
}
/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON(response: Response) {
  if (response.status === 204 || response.status === 205) {
    return null;
  }
  return response.json();
}

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
async function checkStatus(response: Response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const json = await response.json();
  // console.log(json.message);
  throw new Error(json.message);
}

// const baseURL = 'https://api.meomeo.io/';
// const baseURL = 'http://45.77.47.0:8000/';
const baseURL = 'http://meomeo.io:8000/';
//const baseURL = 'http://localhost:8000/';
/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {object}           The response data
 */
export async function request(
  url: string,
): Promise<{} | { err: ResponseError }> {
  const fetchResponse = await fetch(baseURL + url);
  const response = await checkStatus(fetchResponse);
  return parseJSON(response);
}

export async function post(
  url: string,
  body: any,
): Promise<{} | { err: ResponseError }> {
  const fetchResponse = await fetch(baseURL + url, {
    method: 'POST',
    redirect: 'follow',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
    },
  });
  const response = await checkStatus(fetchResponse);
  return parseJSON(response);
}

export async function postWithBearer(
  url: string,
  body: any,
  bearer: any,
): Promise<{} | { err: ResponseError }> {
  const fetchResponse = await fetch(baseURL + url, {
    method: 'POST',
    redirect: 'follow',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + bearer,
    },
  });
  const response = await checkStatus(fetchResponse);
  return parseJSON(response);
}
