export function addCommas(number: number) {
  return number.toLocaleString(undefined, { maximumFractionDigits: 2 });
}
