import { B } from 'app/components/B';
import { Footer } from 'app/components/Footer';
import { NavBar } from 'app/components/NavBar';
import { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { useParams } from 'react-router-dom';
import useStore from 'store/main';
import Table from 'react-bootstrap/Table';

import styled from 'styled-components/macro';

export function SearchPage() {
  const [loading, setLoading] = useState(false);
  let { id } = useParams();

  const searchPayment = useStore(state => state.searchPayment);
  const payment = useStore(state => state.payment);
  const [payment_id, setPaymentId] = useState('');

  useEffect(() => {
    const userString = sessionStorage.getItem('user');
    if (userString != null) {
      let user = JSON.parse(userString);
      const currentTimeStamp = Date.now();
      if (currentTimeStamp >= user.expired_in) {
        sessionStorage.removeItem('user');
        window.location.href = '/';
      }
    } else {
      window.location.href = '/login';
    }
  }, []);

  function statusLabel(status) {
    if (status === 'finished') {
      return 'Finished';
    }
    return 'Checking Status';
  }

  function downloadTXT() {
    if (payment?.sendMailUrlTxt) {
      window.open(payment?.sendMailUrlTxt, '_self');
    }
  }

  function downloadExcel() {
    if (payment?.sendMailUrlXlsx) {
      window.open(payment?.sendMailUrlXlsx, '_self');
    }
  }
  function goHome() {
    window.location.href = '/';
  }
  return (
    <>
      <Helmet>
        <title>Searching</title>
        <meta name="description" content="Tìm kiếm" />
      </Helmet>
      <NavBar />
      <Wrapper>
        <Title>Order searching</Title>
        <Form>
          <Row>
            <RowLabel>Payment Id:</RowLabel>
            <Input
              type={'payment_id'}
              onChange={e => setPaymentId(e.target.value as string)}
            />
          </Row>
          <Button
            type="button"
            onClick={async () => {
              setLoading(true);
              await searchPayment(payment_id);
              setLoading(false);
            }}
            disabled={loading}
          >
            Search
          </Button>
        </Form>
      </Wrapper>
      <WrapperResult>
        <DivTable>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Column Name</th>
                <th>Column Value</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  {' '}
                  <RowLabel>Total amount :</RowLabel>
                </td>
                <td>
                  {' '}
                  <RowLabel2>{payment?.pay_amount}</RowLabel2>
                </td>
              </tr>
              <tr>
                <td>
                  {' '}
                  <RowLabel>Total amount actually received:</RowLabel>
                </td>
                <td>
                  {' '}
                  <RowLabel2>{payment?.amount_received}</RowLabel2>
                </td>
              </tr>
              <tr>
                <td>
                  {' '}
                  <RowLabel>Payment currency:</RowLabel>
                </td>
                <td>
                  {' '}
                  <RowLabel2>{payment?.pay_currency}</RowLabel2>
                </td>
              </tr>
              <tr>
                <td>
                  {' '}
                  <RowLabel>Date:</RowLabel>
                </td>
                <td>
                  {' '}
                  <RowLabel2>{payment?.created_at}</RowLabel2>
                </td>
              </tr>
              <tr>
                <td>
                  {' '}
                  <RowLabel>Send TXT file:</RowLabel>
                </td>
                <td>
                  <Button
                    type="button"
                    onClick={downloadExcel}
                    disabled={!payment?.sendMailUrlXlsx}
                  >
                    Download TXT
                  </Button>
                </td>
              </tr>
              <tr>
                <td>
                  {' '}
                  <RowLabel>Send Excel file:</RowLabel>
                </td>
                <td>
                  <Button
                    type="button"
                    onClick={downloadExcel}
                    disabled={!payment?.sendMailUrlXlsx}
                  >
                    Download excel
                  </Button>
                </td>
              </tr>
            </tbody>
          </Table>
        </DivTable>
      </WrapperResult>
      <Footer />
    </>
  );
}

const Wrapper = styled.div`
  display: flex;
  background-color: #f5f5f5;
  align-items: center;
  justify-content: start;
  flex-direction: column;
  gap: 2rem;
  padding-bottom: 10px;
`;
const WrapperResult = styled.div`
  display: flex;
  background-color: #f5f5f5;
  align-items: center;
  justify-content: start;
  flex-direction: column;
  gap: 2rem;
  padding-bottom: 150px;
`;

const Title = styled.div`
  font-weight: 500;
  text-align: center;
  color: ${p => p.theme.text};
  font-size: 2.5rem;
  margin-top: 3rem;
`;

const Form = styled.form`
  background: #ffffff;
  box-shadow: 0px 0px 10px rgba(234, 67, 53, 0.1);
  border-radius: 5px;
  width: 100%;
  padding: 30px;
  @media (min-width: 900px) {
    width: 900px;
    padding: 35px 56px;
  }
`;

const DivTable = styled.form`
  background: #ffffff;
  box-shadow: 0px 0px 10px rgba(234, 67, 53, 0.1);
  border-radius: 5px;
  width: 100%;
  padding: 30px;
  @media (min-width: 900px) {
    width: 900px;
    padding: 35px 56px;
  }
`;

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 1rem;
`;

const Field = styled.div`
  display: flex;
  justify-content: space-between;
  height: 45px;
  align-items: center;
  margin-bottom: 0.5rem;
`;

const Input = styled.input`
  color: ${p => p.theme.text};
  height: 45px;
  border: 1px solid #dbdbdb;
  border-radius: 3px;
  flex-grow: 1;
  padding: 0 1rem;
`;

const RowLabel = styled.span`
  color: #828282;
  line-height: 1.2rem;
  flex-basis: 45%;
`;

const RowLabel2 = styled.span`
  color: #828282;
  line-height: 1.2rem;
  flex-basis: 45%;
  margin-left: 20px;
`;

const Price = styled.span`
  font-weight: 700;
  font-size: 20px;
  line-height: 27px;
  text-align: right;

  color: #ea4335;
`;

const Button = styled.button`
  width: 100%;
  height: 45px;

  background: #ea4335;
  border-radius: 5px;
  font-weight: 700;
  font-size: 14px;
  line-height: 18px;
  text-align: center;
  text-transform: uppercase;
  border: none;
  color: #ffffff;
  cursor: pointer;
  &[disabled] {
    opacity: 0.5;
  }
`;
