import { B } from 'app/components/B';
import { Footer } from 'app/components/Footer';
import { NavBar } from 'app/components/NavBar';
import { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { useParams } from 'react-router-dom';
import useStore from 'store/main';

import styled from 'styled-components/macro';

export function CheckoutPage() {
  let { id } = useParams();
  const [loading, setLoading] = useState(false);
  const getProduct = useStore(state => state.getProduct);
  const selectedProduct = useStore(state => state.selectedProduct);
  const invoice = useStore(state => state.invoice);
  const createInvoice = useStore(state => state.createInvoice);
  const createInvoiceTraTruoc = useStore(state => state.createInvoiceTraTruoc);
  const [quantity, setQuantity] = useState(1);
  const [email, setEmail] = useState('');
  useEffect(() => {
    getProduct(id);
  }, [getProduct, id]);

  useEffect(() => {
    const userString = sessionStorage.getItem('user');
    if (userString != null) {
      let user = JSON.parse(userString);
      const currentTimeStamp = Date.now();
      if (currentTimeStamp >= user.expires_in) {
        sessionStorage.removeItem('user');
        window.location.href = '/';
      }
    }
    if (!invoice) {
      return;
    }
    if (!invoice.invoice_url) {
      return;
    }

    if (userString != null) {
      window.location.href = invoice.success_url;
      return;
    } else {
      window.location.href = invoice.invoice_url;
      return;
    }
  }, [invoice]);

  let user = useStore(state => state.user);
  const userString = sessionStorage.getItem('user');
  if (userString != null) {
    user = JSON.parse(userString);
    return (
      <>
        <Helmet>
          <title>Checkout</title>
          <meta name="description" content="Checkout" />
        </Helmet>
        <NavBar />
        <Wrapper>
          <Title>
            {selectedProduct?.description}
            <br />

            <B>({selectedProduct?.price + ' USDT'})</B>
          </Title>

          <Form>
            <Row>
              <RowLabel>Account creation date:</RowLabel>
              <b>{selectedProduct?.date_create}</b>
            </Row>
            <Row>
              <RowLabel>Account creation device:</RowLabel>
              <b>{selectedProduct?.device_create}</b>
            </Row>
            <Row>
              <RowLabel>Available quantity:</RowLabel>
              <b>{selectedProduct?.in_stock}</b>
            </Row>
            <Row>
              <RowLabel>Has Youtube:</RowLabel>
              <b>{selectedProduct?.has_youtube}</b>
            </Row>
            <Row>
              <RowLabel>Country:</RowLabel>
              <b>{selectedProduct?.ip_create}</b>
            </Row>
            <Field>
              <RowLabel>Buying quantity:</RowLabel>
              <Input
                type={'number'}
                value={quantity}
                min={1}
                max={selectedProduct?.in_stock ?? 0}
                onChange={e => setQuantity(e.target.value as unknown as number)}
              />
            </Field>
            <Field>
              <RowLabel>Payment Method:</RowLabel>

              <b>USDT TRC20</b>
            </Field>
            <Field>
              <RowLabel>Total Amount</RowLabel>
              <Price>{quantity * (selectedProduct?.price ?? 0)} USD</Price>
            </Field>
            <hr />
            <p></p>
            <Button
              type="button"
              onClick={async () => {
                setLoading(true);

                await createInvoiceTraTruoc(
                  selectedProduct?.id,
                  quantity * 1,
                  user?.email,
                  null,
                  1,
                );

                setLoading(false);
              }}
              disabled={loading}
            >
              Checkout
            </Button>
          </Form>
        </Wrapper>
        <Footer />
      </>
    );
  } else {
    return (
      <>
        <Helmet>
          <title>Checkout</title>
          <meta name="description" content="Checkout" />
        </Helmet>
        <NavBar />
        <Wrapper>
          <Title>
            {selectedProduct?.description}
            <br />

            <B>({selectedProduct?.price + ' USDT'})</B>
          </Title>

          <Form>
            <Row>
              <RowLabel>Account creation date:</RowLabel>
              <b>{selectedProduct?.date_create}</b>
            </Row>
            <Row>
              <RowLabel>Account creation device:</RowLabel>
              <b>{selectedProduct?.device_create}</b>
            </Row>
            <Row>
              <RowLabel>Available quantity:</RowLabel>
              <b>{selectedProduct?.in_stock}</b>
            </Row>
            <Row>
              <RowLabel>Has Youtube:</RowLabel>
              <b>{selectedProduct?.has_youtube}</b>
            </Row>
            <Row>
              <RowLabel>Country:</RowLabel>
              <b>{selectedProduct?.ip_create}</b>
            </Row>
            <Field>
              <RowLabel>Email:</RowLabel>
              <Input
                type={'email'}
                onChange={e => setEmail(e.target.value as string)}
              />
            </Field>
            <Field>
              <RowLabel>Buying quantity:</RowLabel>
              <Input
                type={'number'}
                value={quantity}
                min={1}
                max={selectedProduct?.in_stock ?? 0}
                onChange={e => setQuantity(e.target.value as unknown as number)}
              />
            </Field>
            <Field>
              <RowLabel>Payment Method:</RowLabel>

              <b>USDT TRC20</b>
            </Field>
            <Field>
              <RowLabel>Total Amount:</RowLabel>
              <Price>{quantity * (selectedProduct?.price ?? 0)} USD</Price>
            </Field>
            <hr />
            <p></p>
            <Button
              type="button"
              onClick={async () => {
                setLoading(true);
                await createInvoice(
                  selectedProduct?.id,
                  quantity * 1,
                  email,
                  null,
                  0,
                );
                setLoading(false);
              }}
              disabled={loading}
            >
              Checkout
            </Button>
          </Form>
        </Wrapper>
        <Footer />
      </>
    );
  }
}

const Wrapper = styled.div`
  display: flex;
  background-color: #f5f5f5;
  align-items: center;
  justify-content: start;
  flex-direction: column;
  gap: 2rem;
  padding-bottom: 150px;
`;

const Title = styled.div`
  font-weight: 500;
  text-align: center;
  color: ${p => p.theme.text};
  font-size: 2.5rem;
  margin-top: 3rem;
`;

const Form = styled.form`
  background: #ffffff;
  box-shadow: 0px 0px 10px rgba(234, 67, 53, 0.1);
  border-radius: 5px;
  width: 100%;
  padding: 30px;
  @media (min-width: 768px) {
    width: 680px;
    padding: 35px 56px;
  }
`;

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 1rem;
`;

const Field = styled.div`
  display: flex;
  justify-content: space-between;
  height: 45px;
  align-items: center;
  margin-bottom: 0.5rem;
`;

const Input = styled.input`
  color: ${p => p.theme.text};
  height: 45px;
  border: 1px solid #dbdbdb;
  border-radius: 3px;
  flex-grow: 1;
  padding: 0 1rem;
`;

const RowLabel = styled.span`
  color: #828282;
  line-height: 1.2rem;
  flex-basis: 45%;
`;

const Price = styled.span`
  font-weight: 700;
  font-size: 20px;
  line-height: 27px;
  text-align: right;

  color: #ea4335;
`;

const Button = styled.button`
  width: 100%;
  height: 45px;

  background: #ea4335;
  border-radius: 5px;
  font-weight: 700;
  font-size: 14px;
  line-height: 18px;
  text-align: center;
  text-transform: uppercase;
  border: none;
  color: #ffffff;
  cursor: pointer;
  &[disabled] {
    opacity: 0.5;
  }
`;
