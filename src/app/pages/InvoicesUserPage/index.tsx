import { Footer } from 'app/components/Footer';
import { NavBar } from 'app/components/NavBar';
import { useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { useParams } from 'react-router-dom';
import useStore from 'store/main';
import { LoadingIndicator } from 'app/components/LoadingIndicator';
import styled from 'styled-components/macro';
import { Table } from 'react-bootstrap';

function checkTrangThai(status) {
  if (status === 'finished') {
    return (
      <>
        <Finished>{status}</Finished>
      </>
    );
  } else {
    return (
      <>
        <Pending>{status}</Pending>
      </>
    );
  }
}

export function InvoicesUserPage() {
  const getDonHangByUserId = useStore(state => state.getDonHangByUserId);
  const danhsachdonhang = useStore(state => state.danhSachDonHang);
  useEffect(() => {
    const userString = sessionStorage.getItem('user');
    if (userString != null) {
      let user = JSON.parse(userString);
      const currentTimeStamp = Date.now();
      if (currentTimeStamp >= user.expired_in) {
        window.location.href = '/login';
      }
      getDonHangByUserId(user.id);
    } else {
      window.location.href = '/login';
    }
  }, []);
  return (
    <>
      <Helmet>
        <title>Order List</title>
        <meta name="description" content="order success" />
      </Helmet>
      <NavBar />
      <WrapperResult>
        <Title>Order List</Title>
        <DivTable>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Gmail Type</th>
                <th>Quantity</th>
                <th>Total Amount</th>
                <th>Status</th>
                <th>Date</th>
                <th></th>
              </tr>
            </thead>
            {danhsachdonhang ? (
              <tbody>
                {danhsachdonhang.map((p, index) => (
                  <tr>
                    <td>{index + 1}</td>
                    <td>{p.description}</td>
                    <td>{p.quantity}</td>
                    <td>{`${p.price_amount} ${p.price_currency}`}</td>
                    <td>{checkTrangThai(p.payment_status)}</td>
                    <td>{p.date_created}</td>
                    <td>
                      <a href={p.link}>Detail</a>
                    </td>
                  </tr>
                ))}
              </tbody>
            ) : (
              <tbody>
                <tr>
                  <td>
                    <Loading>
                      <LoadingIndicator />
                    </Loading>
                  </td>
                  <td>
                    <Loading>
                      <LoadingIndicator />
                    </Loading>
                  </td>
                  <td>
                    <Loading>
                      <LoadingIndicator />
                    </Loading>
                  </td>
                  <td>
                    <Loading>
                      <LoadingIndicator />
                    </Loading>
                  </td>
                  <td>
                    <Loading>
                      <LoadingIndicator />
                    </Loading>
                  </td>
                  <td>
                    <Loading>
                      <LoadingIndicator />
                    </Loading>
                  </td>
                  <td>
                    <Loading>
                      <LoadingIndicator />
                    </Loading>
                  </td>
                </tr>
              </tbody>
            )}
          </Table>
        </DivTable>
      </WrapperResult>
      <Footer />
    </>
  );
}

const Wrapper = styled.div`
  display: flex;
  background-color: #f5f5f5;
  align-items: center;
  justify-content: start;
  flex-direction: column;
  gap: 2rem;
  padding-bottom: 10px;
`;
const WrapperResult = styled.div`
  display: flex;
  background-color: #f5f5f5;
  align-items: center;
  justify-content: start;
  flex-direction: column;
  gap: 2rem;
  padding-bottom: 150px;
`;

const Title = styled.div`
  font-weight: 500;
  text-align: center;
  color: ${p => p.theme.text};
  font-size: 2.5rem;
  margin-top: 3rem;
`;

const DivTable = styled.form`
  background: #ffffff;
  box-shadow: 0px 0px 10px rgba(234, 67, 53, 0.1);
  border-radius: 5px;
  width: 100%;
  padding: 30px;
  @media (min-width: 1200px) {
    width: 1200px;
    padding: 35px 56px;
  }
`;

const Finished = styled.span`
  font-weight: 700;
  font-size: 20px;
  line-height: 27px;
  text-align: right;

  color: green;
`;

const Pending = styled.span`
  font-weight: 700;
  font-size: 20px;
  line-height: 27px;
  text-align: right;

  color: red;
`;

const Loading = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 200px;
`;
