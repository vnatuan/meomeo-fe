import { B } from 'app/components/B';
import { Footer } from 'app/components/Footer';
import { NavBar } from 'app/components/NavBar';
import { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { useParams } from 'react-router-dom';
import useStore from 'store/main';

import styled from 'styled-components/macro';

export function ResetPassword() {
  const [loading, setLoading] = useState(false);

  const [email, setEmail] = useState('');
  const resetPassword = useStore(state => state.resetPassword);

  let user = useStore(state => state.user);
  const userString = sessionStorage.getItem('user');
  if (userString != null) {
    user = JSON.parse(userString);
  }

  useEffect(() => {
    const userString = sessionStorage.getItem('user');
    if (userString != null) {
      let user = JSON.parse(userString);
      const currentTimeStamp = Date.now();
      if (currentTimeStamp >= user.expires_in) {
        console.log('here');
        sessionStorage.removeItem('user');
        window.location.href = '/';
      }
    }
  }, []);

  return (
    <>
      <Helmet>
        <title>Reset Password</title>
        <meta name="description" content="Change Password" />
      </Helmet>
      <NavBar />
      <Wrapper>
        <Title>Reset Password</Title>
        <Form>
          <Row>
            <RowLabel>Email:</RowLabel>
            <Input
              type={'email'}
              onChange={e => setEmail(e.target.value as string)}
            />
          </Row>
          <Button
            type="button"
            onClick={async () => {
              setLoading(true);
              await resetPassword(setEmail);
              setLoading(false);
            }}
            disabled={loading}
          >
            Reset Password
          </Button>
        </Form>
      </Wrapper>
      <Footer />
    </>
  );
}

const Wrapper = styled.div`
  display: flex;
  background-color: #f5f5f5;
  align-items: center;
  justify-content: start;
  flex-direction: column;
  gap: 2rem;
  padding-bottom: 150px;
`;

const Title = styled.div`
  font-weight: 500;
  text-align: center;
  color: ${p => p.theme.text};
  font-size: 2.5rem;
  margin-top: 3rem;
`;

const Form = styled.form`
  background: #ffffff;
  box-shadow: 0px 0px 10px rgba(234, 67, 53, 0.1);
  border-radius: 5px;
  width: 100%;
  padding: 30px;
  @media (min-width: 768px) {
    width: 680px;
    padding: 35px 56px;
  }
`;

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 1rem;
`;

const Field = styled.div`
  display: flex;
  justify-content: space-between;
  height: 45px;
  align-items: center;
  margin-bottom: 0.5rem;
`;

const Input = styled.input`
  color: ${p => p.theme.text};
  height: 45px;
  border: 1px solid #dbdbdb;
  border-radius: 3px;
  flex-grow: 1;
  padding: 0 1rem;
`;

const RowLabel = styled.span`
  color: #828282;
  line-height: 1.2rem;
  flex-basis: 45%;
`;

const Price = styled.span`
  font-weight: 700;
  font-size: 20px;
  line-height: 27px;
  text-align: right;

  color: #ea4335;
`;

const Button = styled.button`
  width: 100%;
  height: 45px;

  background: #ea4335;
  border-radius: 5px;
  font-weight: 700;
  font-size: 14px;
  line-height: 18px;
  text-align: center;
  text-transform: uppercase;
  border: none;
  color: #ffffff;
  cursor: pointer;
  &[disabled] {
    opacity: 0.5;
  }
`;
