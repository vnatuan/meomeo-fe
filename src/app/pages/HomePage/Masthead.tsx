import * as React from 'react';
import styled from 'styled-components/macro';
import { ReactComponent as HeaderBg } from './assets/header-bg.svg';
import HeaderImage from './assets/header-image.png';
import { Lead } from './components/Lead';
import { Title } from './components/Title';
export function Masthead() {
  return (
    <Wrapper>
      <Banner>
        <HeaderBg className="header-bg" />
      </Banner>
      <HeadContent>
        <div className="col">
          <Title>acc tốt giá tốt</Title>
          <Lead>
            Cum sociis natoque penatibus et magnis dis parturient montes,
            nascetur ridiculus mus. Donec quam felis, ultricies nec,
            pellentesque eu, pretium quis, sem.
          </Lead>
        </div>
        <HeadImage className='col'>
          <img src={HeaderImage} alt="meomeo header"/>
        </HeadImage>
      </HeadContent>
    </Wrapper>
  );
}

const Wrapper = styled.main`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  min-height: 320px;
  .header-bg {
    position: absolute;
    z-index: -1;
    top: 0;
  }
`;

const HeadContent = styled.div`
  display: flex;
  padding: 0 1.5rem;
  justifi-content: space-between;
  align-items: center;
  h1 {
    color: white;
    text-transform: uppercase;
  }
  p {
    color: white;
  }

  @media (min-width: 768px) {
    margin-top: 40px;
  }
`;

const Banner = styled.div`
  position: absolute;
  width: 100vw;
  top: 0;
  height: 100%;
  overflow: hidden;
`;

const HeadImage = styled.div`
  display: none;

  @media (min-width: 768px) {
    display: block;
  }
`;
