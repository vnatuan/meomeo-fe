import { Footer } from 'app/components/Footer';
import { NavBar } from 'app/components/NavBar';
import { PageWrapper } from 'app/components/PageWrapper';
import { Helmet } from 'react-helmet-async';
import 'reactjs-popup/dist/index.css';
import useStore from 'store/main';
import { Masthead } from './Masthead';
import { Products } from './Products';
export function HomePage() {
  const errorMessage = useStore(state => state.errorMessage);
  return (
    <>
      <Helmet>
        <title>Home Page</title>
        <meta
          name="description"
          content="A React Boilerplate application homepage"
        />
      </Helmet>
      <NavBar />
      <PageWrapper>
        {/* <Masthead /> */}
        <Products />
      </PageWrapper>
      <Footer />
    </>
  );
}
