import { A } from 'app/components/A';
import { LoadingIndicator } from 'app/components/LoadingIndicator';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import useStore from 'store/main';
import styled from 'styled-components/macro';
import { Title } from '../components/Title';
import { Product } from './Product';

export function Products() {
  const { t } = useTranslation();
  const getProducts = useStore(state => state.getProducts);
  const products = useStore(state => state.products);
  useEffect(() => {
    getProducts();
  }, []);

  return (
    <>
      <Title as="h2">
        Gmail list
        <A> of Meomeo.io</A>
      </Title>
      {products ? (
        <List>
          {products.map((p, index) => (
            <Product data={p} key={index} />
          ))}
        </List>
      ) : (
        <Loading>
          <LoadingIndicator />
        </Loading>
      )}
    </>
  );
}

const List = styled.ul`
  padding: 0;
  margin: 53px 0 122px 0;
  min-height: 200px;
`;

const Loading = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 200px;
`;
