import { Button } from 'app/components/Button';
import { Region } from 'app/components/Region';
import { ProductModel } from 'models/product';
import * as React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components/macro';
import { addCommas } from 'utils/strings';
import { ReactComponent as DeviceIcon } from './assets/device.svg';
import { ReactComponent as FolderIcon } from './assets/folder.svg';
import { ReactComponent as TimeIcon } from './assets/time.svg';

export function Product(props) {
  const { description, device_create, date_create, in_stock, price, id } =
    props.data as ProductModel;
  return (
    <Wrapper>
      <Region region={'VN'} />
      <Content>
        <PTitle>
          {description} <span>({addCommas(price)} USD)</span>
        </PTitle>
        <ProductInfo>
          <Span>
            <TimeIcon /> Date Reg: &nbsp;<Bold>{date_create}</Bold>
          </Span>
          <Span>
            <DeviceIcon /> Device: &nbsp;
            <Bold>{device_create}</Bold>
          </Span>
          {/* <Span>
            <YoutubeIcon /> Có kênh Youtube
          </Span> */}
          <Span>
            <FolderIcon /> Quantity: <Quantity>{in_stock}</Quantity>{' '}
            <EmailType></EmailType>
          </Span>
        </ProductInfo>
      </Content>
      <Actions>
        <Link to={process.env.PUBLIC_URL + `/checkout/${id}`}>
          <Button label={'Buy Now'} disabled={in_stock <= 0} />
        </Link>
      </Actions>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  background: white;
  box-shadow: 0px 0px 10px #00000052;
  border-radius: 5px;
  padding: 1rem;
  margin: 1rem;
  cursor: pointer;
  justify-content: center;

  .feature-icon {
    width: 6.25rem;
    height: 6.25rem;
    margin-right: 23px;
    flex-shrink: 0;
  }

  @media (min-width: 768px) {
    cursor: auto;
  }
`;

const PTitle = styled.h3`
  font-size: 1.15rem;
  margin: 0;
  color: ${p => p.theme.text};
  span {
    color: rgba(215, 113, 88, 1);
  }
`;

const Content = styled.div`
  flex: 1;
`;

const Actions = styled.div`
  margin-top: 1rem;

  @media (min-width: 768px) {
    margin-top: 0rem;
  }
`;

const Span = styled.span`
  display: inline-flex;
  align-items: center;
  margin-right: 2rem;
  svg {
    margin-right: 0.5rem;
  }
`;

export const ProductInfo = styled.p`
  font-size: 1rem;
  line-height: 1.5;
  color: ${p => p.theme.textSecondary};
  margin: 0.625rem 0 0 0;
`;

export const Quantity = styled.span`
  color: #ef3131;
  font-size: 1.2rem;
  font-weight: 700;
  margin: 0 0.5rem;
`;

export const EmailType = styled.span`
  color: black;
  font-weight: 700;
`;

export const Bold = styled.b`
  color: #1a1a1a;
`;
