import { B } from 'app/components/B';
import { Footer } from 'app/components/Footer';
import { NavBar } from 'app/components/NavBar';
import { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { useParams } from 'react-router-dom';
import useStore from 'store/main';
import Table from 'react-bootstrap/Table';

import styled from 'styled-components/macro';

export function CustomersPage() {
  const [loading, setLoading] = useState(false);
  let { id } = useParams();

  const searchCustomer = useStore(state => state.searchCustomer);
  const customer = useStore(state => state.customer);
  const [customer_email, setCustomerEmail] = useState('');
  const [amount, setAmount] = useState('');
  const addAmountAdmin = useStore(state => state.addAmountAdmin);

  useEffect(() => {
    const userString = sessionStorage.getItem('user');
    if (userString != null) {
      let user = JSON.parse(userString);
      const currentTimeStamp = Date.now();
      if (currentTimeStamp >= user.expired_in) {
        sessionStorage.removeItem('user');
        window.location.href = '/';
      }
    } else {
      window.location.href = '/login';
    }
  }, []);

  function statusLabel(status) {
    if (status === 'finished') {
      return 'Finished';
    }
    return 'Checking Status';
  }

  function goHome() {
    window.location.href = '/';
  }
  return (
    <>
      <Helmet>
        <title>Searching</title>
        <meta name="description" content="Tìm kiếm" />
      </Helmet>
      <NavBar />
      <Wrapper>
        <Title>Customer searching (By Email)</Title>
        <Form>
          <Row>
            <RowLabel>Customer Email:</RowLabel>
            <Input
              type={'customer_email'}
              onChange={e => setCustomerEmail(e.target.value as string)}
            />
          </Row>
          <Button
            type="button"
            onClick={async () => {
              setLoading(true);
              await searchCustomer(customer_email);
              setLoading(false);
            }}
            disabled={loading}
          >
            Search
          </Button>
        </Form>
      </Wrapper>
      <WrapperResult>
        <DivTable>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Column Name</th>
                <th>Column Value</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  {' '}
                  <RowLabel>Name :</RowLabel>
                </td>
                <td>
                  {' '}
                  <RowLabel2>{customer?.name}</RowLabel2>
                </td>
              </tr>
              <tr>
                <td>
                  {' '}
                  <RowLabel>Email :</RowLabel>
                </td>
                <td>
                  {' '}
                  <RowLabel2>{customer?.email}</RowLabel2>
                </td>
              </tr>
              <tr>
                <td>
                  {' '}
                  <RowLabel>Total amount:</RowLabel>
                </td>
                <td>
                  {' '}
                  <RowLabel2>
                    {customer?.total_amount} {customer?.currency_code}
                  </RowLabel2>
                </td>
              </tr>
              <tr>
                <td>
                  {' '}
                  <RowLabel>Add Amount (USD):</RowLabel>
                </td>
                <td>
                  <Row>
                    <Input
                      type={'amount'}
                      onChange={e => setAmount(e.target.value as string)}
                      name="amount"
                    />
                  </Row>
                  <Button
                    type="button"
                    onClick={async () => {
                      setLoading(true);
                      await addAmountAdmin(customer?.id, amount);
                      setLoading(false);
                      await searchCustomer(customer?.email);
                    }}
                  >
                    Add Amount
                  </Button>
                </td>
              </tr>
            </tbody>
          </Table>
        </DivTable>
      </WrapperResult>
      <Footer />
    </>
  );
}

const Wrapper = styled.div`
  display: flex;
  background-color: #f5f5f5;
  align-items: center;
  justify-content: start;
  flex-direction: column;
  gap: 2rem;
  padding-bottom: 10px;
`;
const WrapperResult = styled.div`
  display: flex;
  background-color: #f5f5f5;
  align-items: center;
  justify-content: start;
  flex-direction: column;
  gap: 2rem;
  padding-bottom: 150px;
`;

const Title = styled.div`
  font-weight: 500;
  text-align: center;
  color: ${p => p.theme.text};
  font-size: 2.5rem;
  margin-top: 3rem;
`;

const Form = styled.form`
  background: #ffffff;
  box-shadow: 0px 0px 10px rgba(234, 67, 53, 0.1);
  border-radius: 5px;
  width: 100%;
  padding: 30px;
  @media (min-width: 900px) {
    width: 900px;
    padding: 35px 56px;
  }
`;

const DivTable = styled.form`
  background: #ffffff;
  box-shadow: 0px 0px 10px rgba(234, 67, 53, 0.1);
  border-radius: 5px;
  width: 100%;
  padding: 30px;
  @media (min-width: 900px) {
    width: 900px;
    padding: 35px 56px;
  }
`;

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 1rem;
`;

const Field = styled.div`
  display: flex;
  justify-content: space-between;
  height: 45px;
  align-items: center;
  margin-bottom: 0.5rem;
`;

const Input = styled.input`
  color: ${p => p.theme.text};
  height: 45px;
  border: 1px solid #dbdbdb;
  border-radius: 3px;
  flex-grow: 1;
  padding: 0 1rem;
`;

const RowLabel = styled.span`
  color: #828282;
  line-height: 1.2rem;
  flex-basis: 45%;
`;

const RowLabel2 = styled.span`
  color: #828282;
  line-height: 1.2rem;
  flex-basis: 45%;
  margin-left: 20px;
`;

const Price = styled.span`
  font-weight: 700;
  font-size: 20px;
  line-height: 27px;
  text-align: right;

  color: #ea4335;
`;

const Button = styled.button`
  width: 100%;
  height: 45px;

  background: #ea4335;
  border-radius: 5px;
  font-weight: 700;
  font-size: 14px;
  line-height: 18px;
  text-align: center;
  text-transform: uppercase;
  border: none;
  color: #ffffff;
  cursor: pointer;
  &[disabled] {
    opacity: 0.5;
  }
`;
