import { B } from 'app/components/B';
import { Footer } from 'app/components/Footer';
import { NavBar } from 'app/components/NavBar';
import { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { useParams } from 'react-router-dom';
import useStore from 'store/main';
import Table from 'react-bootstrap/Table';

import styled from 'styled-components/macro';

export function UserProfile() {
  const [loading, setLoading] = useState(false);
  let user = useStore(state => state.user);
  const userString = sessionStorage.getItem('user');
  if (userString != null) {
    user = JSON.parse(userString);
  }

  useEffect(() => {
    const userString = sessionStorage.getItem('user');
    if (userString != null) {
      let user = JSON.parse(userString);
      const currentTimeStamp = Date.now();
      if (currentTimeStamp >= user.expired_in) {
        sessionStorage.removeItem('user');
        window.location.href = '/';
      }
    } else {
      window.location.href = '/login';
    }
  }, []);

  return (
    <>
      <Helmet>
        <title>User Information</title>
        <meta name="description" content="Thông tin user" />
      </Helmet>
      <NavBar />
      <Wrapper>
        <Title>User Information</Title>
        <DivTable>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Column Name</th>
                <th>Column Value</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  {' '}
                  <RowLabel>Name :</RowLabel>
                </td>
                <td>
                  {' '}
                  <RowLabel2>{user?.name}</RowLabel2>
                </td>
              </tr>
              <tr>
                <td>
                  {' '}
                  <RowLabel>Email:</RowLabel>
                </td>
                <td>
                  {' '}
                  <RowLabel2>{user?.email}</RowLabel2>
                </td>
              </tr>
            </tbody>
          </Table>
        </DivTable>
      </Wrapper>
      <Footer />
    </>
  );
}

const Wrapper = styled.div`
  display: flex;
  background-color: #f5f5f5;
  align-items: center;
  justify-content: start;
  flex-direction: column;
  gap: 2rem;
  padding-bottom: 10px;
`;

const Title = styled.div`
  font-weight: 500;
  text-align: center;
  color: ${p => p.theme.text};
  font-size: 2.5rem;
  margin-top: 3rem;
`;

const DivTable = styled.form`
  background: #ffffff;
  box-shadow: 0px 0px 10px rgba(234, 67, 53, 0.1);
  border-radius: 5px;
  width: 100%;
  padding: 30px;
  @media (min-width: 900px) {
    width: 900px;
    padding: 35px 56px;
  }
`;

const RowLabel = styled.span`
  color: #828282;
  line-height: 1.2rem;
  flex-basis: 45%;
`;

const RowLabel2 = styled.span`
  color: #828282;
  line-height: 1.2rem;
  flex-basis: 45%;
  margin-left: 20px;
`;
