import { Footer } from 'app/components/Footer';
import { NavBar } from 'app/components/NavBar';
import { useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { useState } from 'react';
import useStore from 'store/main';
import { Table } from 'react-bootstrap';
import styled from 'styled-components/macro';
import { LoadingIndicator } from 'app/components/LoadingIndicator';

function checkTrangThai(status) {
  if (status === 'finished') {
    return (
      <>
        <Finished>{status}</Finished>
      </>
    );
  } else {
    return (
      <>
        <Pending>{status}</Pending>
      </>
    );
  }
}

export function AddAmount() {
  const getAddAmountHistoryByUserId = useStore(
    state => state.getAddAmountHistoryByUserId,
  );
  const danhSachAddAmount = useStore(state => state.danhSachAddAmount);
  let user = useStore(state => state.user);
  const [loading, setLoading] = useState(false);
  const [amount, setAmount] = useState(1);
  const addAmount = useStore(state => state.addAmount);
  let naptienInvoice = useStore(state => state.naptienInvoice);

  useEffect(() => {
    const userString = sessionStorage.getItem('user');
    if (userString != null) {
      let userLogin = JSON.parse(userString);
      const currentTimeStamp = Date.now();
      if (currentTimeStamp >= userLogin.expired_in) {
        sessionStorage.removeItem('user');
        window.location.href = '/';
      }
      getAddAmountHistoryByUserId(userLogin.id);
    } else {
      window.location.href = '/';
    }
  }, []);

  useEffect(() => {
    console.log(naptienInvoice);
    if (!naptienInvoice) {
      return;
    }
    if (!naptienInvoice.invoice_url) {
      return;
    }
    window.location.href = naptienInvoice.invoice_url;
  }, [naptienInvoice]);

  const userString = sessionStorage.getItem('user');
  if (userString != null) {
    user = JSON.parse(userString);
  }

  return (
    <>
      <Helmet>
        <title>Deposit money into your account</title>
        <meta name="description" content="order success" />
      </Helmet>
      <NavBar />
      <Wrapper>
        <Title>Deposit money into your account (USD)</Title>
        <Alert>Min = 100 Usd - Max = 100000 Usd</Alert>
        <Form>
          <Field>
            <Input
              type={'number'}
              min={100}
              max={100000}
              onChange={e => setAmount(e.target.value as unknown as number)}
            />
          </Field>
          <hr />
          <p></p>
          <Field>
            <Button
              type="button"
              onClick={async () => {
                setLoading(true);
                await addAmount(user?.email, amount);
                setLoading(false);
              }}
              disabled={loading}
            >
              Deposit (USD)
            </Button>
          </Field>
        </Form>
      </Wrapper>
      <WrapperResult>
        <DivTable>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Deposit Amount</th>
                <th>Status</th>
                <th>Date</th>
              </tr>
            </thead>
            {danhSachAddAmount ? (
              <tbody>
                {danhSachAddAmount.map((p, index) => (
                  <tr>
                    <td>{index + 1}</td>
                    <td>{`${p.price_amount} ${p.price_currency}`}</td>
                    <td>{checkTrangThai(p.payment_status)}</td>
                    <td>{p.date_created}</td>
                  </tr>
                ))}
              </tbody>
            ) : (
              <Loading>
                <LoadingIndicator />
              </Loading>
            )}
          </Table>
        </DivTable>
      </WrapperResult>
      <Footer />
    </>
  );
}

const Wrapper = styled.div`
  display: flex;
  background-color: #f5f5f5;
  align-items: center;
  justify-content: start;
  flex-direction: column;
  gap: 2rem;
  padding-bottom: 10px;
`;

const Form = styled.form`
  background: #ffffff;
  box-shadow: 0px 0px 10px rgba(234, 67, 53, 0.1);
  border-radius: 5px;
  width: 100%;
  padding: 30px;
  @media (min-width: 768px) {
    width: 680px;
    padding: 35px 56px;
  }
`;

const Field = styled.div`
  display: flex;
  justify-content: space-between;
  height: 45px;
  align-items: center;
  margin-bottom: 0.5rem;
`;

const Title = styled.div`
  font-weight: 500;
  text-align: center;
  color: ${p => p.theme.text};
  font-size: 2.5rem;
  margin-top: 3rem;
`;

const Input = styled.input`
  color: ${p => p.theme.text};
  height: 45px;
  border: 1px solid #dbdbdb;
  border-radius: 3px;
  flex-grow: 1;
  padding: 0 1rem;
`;

const Button = styled.button`
  height: 45px;
  background: #ea4335;
  border-radius: 5px;
  font-weight: 700;
  font-size: 14px;
  line-height: 18px;
  text-align: center;
  text-transform: uppercase;
  border: none;
  color: #ffffff;
  padding 14px 40px;
  cursor: pointer;
  &[disabled] {
    opacity: 0.5;
  }

  flex: 0 0 100%;
  @media (min-width: 768px) {
    flex: auto;
  }
`;

const WrapperResult = styled.div`
  display: flex;
  background-color: #f5f5f5;
  align-items: center;
  justify-content: start;
  flex-direction: column;
  gap: 2rem;
  padding-bottom: 150px;
`;

const DivTable = styled.form`
  background: #ffffff;
  box-shadow: 0px 0px 10px rgba(234, 67, 53, 0.1);
  border-radius: 5px;
  width: 100%;
  padding: 30px;
  @media (min-width: 1200px) {
    width: 1200px;
    padding: 35px 56px;
  }
`;

const Finished = styled.span`
  font-weight: 700;
  font-size: 20px;
  line-height: 27px;
  text-align: right;

  color: green;
`;

const Alert = styled.span`
  font-weight: 700;
  font-size: 20px;
  line-height: 27px;
  text-align: right;

  color: red;
`;

const Pending = styled.span`
  font-weight: 700;
  font-size: 20px;
  line-height: 27px;
  text-align: right;

  color: red;
`;

const Loading = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 200px;
`;
