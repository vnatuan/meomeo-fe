import { Footer } from 'app/components/Footer';
import { NavBar } from 'app/components/NavBar';
import { useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { useParams } from 'react-router-dom';
import useStore from 'store/main';

import styled from 'styled-components/macro';

export function InvoicePage() {
  let { id } = useParams();
  const getPayment = useStore(state => state.getPayment);
  const getProduct = useStore(state => state.getProduct);
  const payment = useStore(state => state.payment);
  const product = useStore(state => state.selectedProduct);

  useEffect(() => {
    getPayment(id);
  }, [getPayment, id]);

  useEffect(() => {
    if (payment) {
      getProduct(payment?.id_email_type);
    }
  }, [getProduct, payment]);

  function statusLabel(status) {
    if (status === 'finished') {
      return 'Finished';
    }
    return 'Checking Status';
  }

  function downloadTXT() {
    if (payment?.sendMailUrlTxt) {
      window.open(payment?.sendMailUrlTxt, '_self');
    }
  }

  function downloadExcel() {
    if (payment?.sendMailUrlXlsx) {
      window.open(payment?.sendMailUrlXlsx, '_self');
    }
  }
  function goHome() {
    window.location.href = '/';
  }
  return (
    <>
      <Helmet>
        <title>Order success</title>
        <meta name="description" content="order success" />
      </Helmet>
      <NavBar />
      <Wrapper>
        <Title>Order details #{payment?.purchase_id}</Title>

        <Container>
          <LeftSide>
            <RowTitle>Order Status</RowTitle>
            <HR />
            <Row>
              <span>{statusLabel(payment?.payment_status)}</span>
              <Quantity>
                Sold {payment?.quantity}/{product?.in_stock} gmail
              </Quantity>
            </Row>

            <ProgressBar />

            <Center>
              You will not be able to download the email list for this order
              after 48 more hours
              <br />
              Please download and store on your computer
            </Center>
            <Buttons>
              <Button
                type="button"
                onClick={downloadTXT}
                disabled={!payment?.sendMailUrlTxt}
              >
                Download TXT
              </Button>
              <Button
                type="button"
                onClick={downloadExcel}
                disabled={!payment?.sendMailUrlXlsx}
              >
                Download excel
              </Button>
              <Button type="button" onClick={goHome}>
                Home
              </Button>
            </Buttons>
          </LeftSide>
          <RightSide>
            <RowBorder>
              <RowTitle>
                <b>Gmail new VN tạo 3-30 ngày</b>
              </RowTitle>
            </RowBorder>
            <RowBorder>
              <RowLabel>Price: </RowLabel>
              <Price>{product?.price} USDT</Price>
            </RowBorder>
            <RowBorder>
              <RowLabel>Account creation date:</RowLabel>
              <b>{product?.date_create}</b>
            </RowBorder>
            <RowBorder>
              <RowLabel>Account creation device:</RowLabel>
              <b>{product?.device_create}</b>
            </RowBorder>
            <RowBorder>
              <RowLabel>Has Youtube:</RowLabel>
              <b>{product?.has_youtube}</b>
            </RowBorder>

            <RowBorder>
              <RowLabel>Country:</RowLabel>
              <b>{product?.ip_create}</b>
            </RowBorder>
            <RowBorder>
              <RowLabel>Buying quantity:</RowLabel>
              <b>{payment?.quantity}</b>
            </RowBorder>

            <RowBorder>
              <RowLabel>Total Amount:</RowLabel>
              <b>
                {payment?.price_amount} {payment?.price_currency}
              </b>
            </RowBorder>
          </RightSide>
        </Container>
      </Wrapper>
      <Footer />
    </>
  );
}

const Wrapper = styled.div`
  display: flex;
  background-color: #f5f5f5;
  align-items: center;
  justify-content: start;
  flex-direction: column;
  gap: 2rem;
  padding-bottom: 20px;
  @media (min-width: 768px) {
    padding-bottom: 150px;
  }
`;

const Container = styled.div`
  display: flex;
  gap: 2rem;
  max-width: 1170px;
  width: 100%;
  flex-wrap: wrap;
  @media (min-width: 768px) {
    flex-wrap: nowrap;
  }
`;

const Title = styled.div`
  font-weight: 500;
  text-align: center;
  color: ${p => p.theme.text};
  font-size: 2.5rem;
  margin-top: 3rem;
`;

const LeftSide = styled.div`
  background: #ffffff;
  box-shadow: 0px 0px 10px 0px #ea43351a;
  border-radius: 5px;
  flex-grow: 1;
  padding: 2rem;
  margin: 0 20px;
  @media (min-width: 768px) {
    margin: auto;
  }
`;
const RightSide = styled.div`
  background: #ffffff;
  box-shadow: 0px 0px 10px 0px #ea43351a;
  border-radius: 5px;
  width: 375px;
  padding: 24px;
  height: 100%;
  margin: 0 20px;
  @media (min-width: 768px) {
    margin: auto;
  }
`;
const Row = styled.div`
  display: flex;
  justify-content: space-between;
  height: 42px;
`;

const RowBorder = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 1rem;
  border-bottom: 1px solid #e3e3e3;
  height: 42px;
`;

const RowTitle = styled.span`
  color: #1e2329;
  font-size: 1.2rem;
  font-weight: 700;
`;

const RowLabel = styled.span`
  color: #828282;
  line-height: 1.2rem;
`;

const Price = styled.span`
  font-weight: 700;
  font-size: 20px;
  line-height: 27px;
  text-align: right;
  color: #ea4335;
`;

const Buttons = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  gap: 14px;
  flex-wrap: wrap;
`;

const Button = styled.button`
  height: 45px;
  background: #ea4335;
  border-radius: 5px;
  font-weight: 700;
  font-size: 14px;
  line-height: 18px;
  text-align: center;
  text-transform: uppercase;
  border: none;
  color: #ffffff;
  padding 14px 40px;
  cursor: pointer;
  &[disabled] {
    opacity: 0.5;
  }

  flex: 0 0 100%;
  @media (min-width: 768px) {
    flex: auto;
  }
`;

const ProgressBar = styled.div`
  width: 100%;
  height: 7px;
  left: 409px;
  top: 357px;

  background: #ea4335;
  border-radius: 100px;
  @media (min-width: 768px) {
    width: 706px;
  }
`;

const HR = styled.hr`
  background-color: #e4e4e4;
  border: none;
  margin: 1rem -2rem;
  height: 1px;
`;
const Center = styled.p`
  text-align: center;
  color: #828282;
  margin: 2rem 0;
`;

const Quantity = styled.span`
  font-size: 0.9rem;
  color: #444444;
  font-weight: 400;
`;
