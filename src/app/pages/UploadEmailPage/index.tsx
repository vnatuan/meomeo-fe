import { B } from 'app/components/B';
import { Footer } from 'app/components/Footer';
import { NavBar } from 'app/components/NavBar';
import { useState } from 'react';
import { Helmet } from 'react-helmet-async';
import useStore from 'store/main';
import styled from 'styled-components/macro';
import { ChangeEvent } from 'react';
import { useEffect } from 'react';

export function UploadEmailPage() {
  const uploadEmailsFile = useStore(state => state.uploadEmailsFile);
  const [file, setFile] = useState<File>();
  const getProducts = useStore(state => state.getProducts);
  const products = useStore(state => state.products);
  const [selectedOption, setSelected] = useState('1');
  useEffect(() => {
    getProducts();
  }, []);

  const handleFileChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      setFile(e.target.files[0]);
    }
  };

  const handleUploadClick = () => {
    if (!file) {
      return;
    }
    const formData = new FormData();
    formData.append('file', file);
    formData.append('type', selectedOption);
    uploadEmailsFile(formData);
  };

  const handleChange = event => {
    console.log(event.target.value);
    setSelected(event.target.value);
  };
  return (
    <>
      <Helmet>
        <title>Upload Email</title>
        <meta name="description" content="Tìm kiếm" />
      </Helmet>
      <NavBar />
      <Wrapper>
        <Title>Upload Email (By File TXT)</Title>
        <Form>
          <Row>
            <RowLabel>Please Select Email Type:</RowLabel>
            <select value={selectedOption} onChange={handleChange}>
              {products.map(o => (
                <option value={o.id}>{o.description}</option>
              ))}
            </select>
          </Row>
          <Row>
            <RowLabel>Email File:</RowLabel>
            <Input type="file" onChange={handleFileChange} />
          </Row>
          <Button type="button" onClick={handleUploadClick}>
            Upload
          </Button>
        </Form>
      </Wrapper>
      <Footer />
    </>
  );
}

const Wrapper = styled.div`
  display: flex;
  background-color: #f5f5f5;
  align-items: center;
  justify-content: start;
  flex-direction: column;
  gap: 2rem;
  padding-bottom: 10px;
`;
const WrapperResult = styled.div`
  display: flex;
  background-color: #f5f5f5;
  align-items: center;
  justify-content: start;
  flex-direction: column;
  gap: 2rem;
  padding-bottom: 150px;
`;

const Title = styled.div`
  font-weight: 500;
  text-align: center;
  color: ${p => p.theme.text};
  font-size: 2.5rem;
  margin-top: 3rem;
`;

const Form = styled.form`
  background: #ffffff;
  box-shadow: 0px 0px 10px rgba(234, 67, 53, 0.1);
  border-radius: 5px;
  width: 100%;
  padding: 30px;
  @media (min-width: 900px) {
    width: 900px;
    padding: 35px 56px;
  }
`;

const DivTable = styled.form`
  background: #ffffff;
  box-shadow: 0px 0px 10px rgba(234, 67, 53, 0.1);
  border-radius: 5px;
  width: 100%;
  padding: 30px;
  @media (min-width: 900px) {
    width: 900px;
    padding: 35px 56px;
  }
`;

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 1rem;
`;

const Field = styled.div`
  display: flex;
  justify-content: space-between;
  height: 45px;
  align-items: center;
  margin-bottom: 0.5rem;
`;

const Input = styled.input`
  color: ${p => p.theme.text};
  height: 45px;
  border: 1px solid #dbdbdb;
  border-radius: 3px;
  flex-grow: 1;
  padding: 0 1rem;
`;

const RowLabel = styled.span`
  color: #828282;
  line-height: 1.2rem;
  flex-basis: 45%;
`;

const RowLabel2 = styled.span`
  color: #828282;
  line-height: 1.2rem;
  flex-basis: 45%;
  margin-left: 20px;
`;

const Price = styled.span`
  font-weight: 700;
  font-size: 20px;
  line-height: 27px;
  text-align: right;

  color: #ea4335;
`;

const Button = styled.button`
  width: 100%;
  height: 45px;

  background: #ea4335;
  border-radius: 5px;
  font-weight: 700;
  font-size: 14px;
  line-height: 18px;
  text-align: center;
  text-transform: uppercase;
  border: none;
  color: #ffffff;
  cursor: pointer;
  &[disabled] {
    opacity: 0.5;
  }
`;
