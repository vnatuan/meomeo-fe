import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import useStore from 'store/main';

export function AppModal() {
  const [show, setShow] = useState(false);
  const [message, setMessage] = useState('');
  const [title, setTitle] = useState('');
  const errorMessage = useStore(state => state.errorMessage);
  const modalTitle = useStore(state => state.modalTitle);

  const handleClose = () => setShow(false);
  useEffect(() => {
    if (errorMessage) {
      setMessage(errorMessage);
      setShow(true);
    }
    if (modalTitle) {
      setTitle(modalTitle);
    }
  }, [errorMessage]);

  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{message}</Modal.Body>
      </Modal>
    </>
  );
}
