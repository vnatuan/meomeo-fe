import React, { memo } from 'react';
import styled from 'styled-components/macro';

import enIcon from './assets/en.png';
import vnIcon from './assets/vn.png';
import usIcon from './assets/us.png';
import geIcon from './assets/ge.png';

type InputProps = React.DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
>;

interface Props extends InputProps {
  region: string;
}

export const Region = memo(({ className, region, ...restOf }: Props) => {
  return (
    <Wrapper className={className}>
      <Icon src={getIcon(region)} />
    </Wrapper>
  );
});

const getIcon = param => {
  switch (param) {
    case 'EN':
      return enIcon;
    case 'US':
      return usIcon;
    case 'GE':
      return geIcon;
    default:
      return vnIcon;
  }
};

const Wrapper = styled.div`
  position: relative;
  width: 70px;
  height: 70px;
  min-width: 70px;
  background: #ffd6d6;
  border-radius: 1000px;
  margin-right: 23px;
`;

const Icon = styled.img`
  position: absolute;
  left: 13px;
  top: 13px;
  width: 44px;
  height: 44px;
`;
