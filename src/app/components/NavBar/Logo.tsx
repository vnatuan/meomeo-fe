import * as React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components/macro';
import { ReactComponent as AppLogo } from './assets/app-logo.svg';

export function Logo() {
  return (
    <Wrapper>
      <Link to="/">
        <AppLogo />
      </Link>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: flex;
  align-items: center;
`;
