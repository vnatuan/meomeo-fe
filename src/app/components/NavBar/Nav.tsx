import * as React from 'react';
import styled from 'styled-components/macro';
import useStore from 'store/main';
import { Menu, MenuItem, MenuDivider } from '@szhsin/react-menu';
import '@szhsin/react-menu/dist/index.css';
import './css/myMenu.css';
import { useEffect } from 'react';

export function Nav() {
  let user = useStore(state => state.user);
  let userAmount = useStore(state => state.userAmount);
  let getUserAmount = useStore(state => state.getUserAmount);
  useEffect(() => {
    const userString = sessionStorage.getItem('user');
    if (userString != null) {
      let userLogin = JSON.parse(userString);
      const currentTimeStamp = Date.now();
      if (currentTimeStamp >= userLogin.expired_in) {
        sessionStorage.removeItem('user');
        window.location.href = '/';
      }
      getUserAmount(user?.id, 'usd');
    }
  }, []);
  console.log(user);
  const userString = sessionStorage.getItem('user');
  if (userString != null) {
    user = JSON.parse(userString);

    if (user?.role === 1) {
      return (
        <Wrapper>
          <Menu menuButton={<Button>{user?.email}</Button>}>
            <MenuItem href="/search-customer/">Customer searching</MenuItem>
            <MenuDivider />
            <MenuItem href="/search-payment/">Order searching</MenuItem>
            <MenuDivider />
            <MenuItem href="/reset-password/">Reset Password</MenuItem>
            <MenuDivider />
            <MenuItem href="/upload-email/">Upload Email</MenuItem>
            <MenuDivider />
            <MenuItem
              href="/"
              onClick={e => {
                sessionStorage.removeItem('user');
              }}
            >
              Logout
            </MenuItem>
          </Menu>
        </Wrapper>
      );
    } else {
      const menuItemClassName = ({ hover }) =>
        hover ? 'my-menuitem-hover' : 'my-menuitem';

      return (
        <Wrapper>
          <Menu menuButton={<Button>{user?.email}</Button>}>
            <MenuItem
              className={menuItemClassName}
              href={`/add-amount`}
            >{`Tài khoản : ${userAmount?.total_amount} USD`}</MenuItem>
            <MenuDivider />
            <MenuItem href={`/invoice/user`}>Orders history</MenuItem>
            <MenuDivider />
            <MenuItem href={`/add-amount`}>Deposit</MenuItem>
            <MenuDivider />
            {/* <MenuItem href="/user-profile">Thông tin user</MenuItem>
            <MenuDivider /> */}
            <MenuItem href="/change-password">Change password</MenuItem>
            <MenuDivider />
            <MenuItem
              href="/"
              onClick={e => {
                sessionStorage.removeItem('user');
              }}
            >
              Logout
            </MenuItem>
          </Menu>
        </Wrapper>
      );
    }
  } else {
    return (
      <Wrapper>
        <Menu menuButton={<Button>Click to open menu</Button>}>
          <MenuItem href="/">Home</MenuItem>
          <MenuDivider />
          <MenuItem href="/login">Login</MenuItem>
          <MenuDivider />
          <MenuItem href="/register">Register</MenuItem>
        </Menu>
      </Wrapper>
    );
  }
}

const Wrapper = styled.nav`
  display: flex;
  margin-right: -1rem;
`;

const Button = styled.button`
  width: 100%;
  height: 45px;
  padding: 0 10px;
  background: #ea4335;
  border-radius: 5px;
  font-weight: 700;
  font-size: 14px;
  line-height: 18px;
  text-align: center;
  text-transform: uppercase;
  border: none;
  color: #ffffff;
  cursor: pointer;
  &[disabled] {
    opacity: 0.5;
  }
`;

const Item = styled.div`
  color: ${p => p.theme.primary};
  cursor: pointer;
  text-decoration: none;
  display: flex;
  padding: 0.25rem 1rem;
  font-size: 0.875rem;
  font-weight: 500;
  align-items: center;

  &:hover {
    opacity: 0.8;
  }

  &:active {
    opacity: 0.4;
  }

  .icon {
    margin-right: 0.25rem;
    width: 1rem;
  }
`;
