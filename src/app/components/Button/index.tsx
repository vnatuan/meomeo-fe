import React, { memo } from 'react';
import styled from 'styled-components/macro';

type InputProps = React.DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
>;

interface Props extends InputProps {
  label: string;
  className?: string;
  full?: Boolean
}

export const Button = memo(
  ({ id, label, className, full, disabled, ...restOf }: Props) => {
    
    return (
      <Wrapper className={className} disabled={disabled}>
        {label}
      </Wrapper>
    );
  },
);

const Wrapper = styled.button`
  width: 142px;
  height: 45px;
  left: 1384px;
  top: 755px;
  background: #ea4335;
  border-radius: 5px;
  color: white;
  border: none;
  cursor: pointer;

  &[disabled] {
    opacity: 0.6;
    cursor: auto;

    &:hover {
      &::before {
        border-color: ${p => p.theme.border};
      }
    }
  }

  &.full {
    width: 100%;
  }
`;
