import styled from 'styled-components/macro';

export const B = styled.b`
  color: ${p => p.theme.primary};
  text-decoration: none;
`;
