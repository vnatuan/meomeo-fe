import styled from 'styled-components/macro';

export const PageWrapper = styled.div`
  width: 100%;
  margin: 0 auto;
  box-sizing: content-box;
  font-size: 0.9rem;

  @media (min-width: 768px) {
    padding: 0 1.5rem;
  }

  @media (min-width: 1024px) {
    padding: 0 1.5rem;
    max-width: 960px;
  }
`;
