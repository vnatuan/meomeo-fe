import * as React from 'react';
import styled from 'styled-components/macro';
import { ReactComponent as AppLogo } from './assets/app-logo.svg';

export function Logo() {
  return (
    <Wrapper>
      <AppLogo />
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  width: 120px;

  @media (min-width: 768px) {
    width: 235px;
  }
`;
