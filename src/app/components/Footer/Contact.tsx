import * as React from 'react';
import styled from 'styled-components/macro';
import { ReactComponent as WAIcon } from './assets/WhatsApp.svg';
import { ReactComponent as TLIcon } from './assets/Telegram.svg';

export function Contact() {
  return (
    <Wrapper>
      <Item
        href="https://wa.me/0356137845"
        target="_blank"
        title="Whatsapp group chat"
        rel="noopener noreferrer"
      >
        <WAIcon />
      </Item>
      <Item
        href="https://web.telegram.org"
        target="_blank"
        title="Telegram meomeo"
        rel="noopener noreferrer"
      >
        <TLIcon />
      </Item>
    </Wrapper>
  );
}

const Wrapper = styled.nav`
  display: flex;
  margin-right: -1rem;
`;

const Item = styled.a`
  color: ${p => p.theme.primary};
  cursor: pointer;
  text-decoration: none;
  display: flex;
  align-items: center;
  margin-right: 1rem;
  width: 30px;

  @media (min-width: 768px) {
    width: 55px;
  }
`;
