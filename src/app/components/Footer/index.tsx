import * as React from 'react';
import styled from 'styled-components/macro';
import { Logo } from './Logo';
import { StyleConstants } from 'styles/StyleConstants';
import { Contact } from './Contact';
import { PageWrapper } from '../PageWrapper';

export function Footer() {
  return (
    <Wrapper>
      <PageWrapper>
        <Logo />
        <Contact />
      </PageWrapper>
    </Wrapper>
  );
}

const Wrapper = styled.footer`
  box-shadow: 0 1px 0 0 ${p => p.theme.borderLight};
  display: flex;
  width: 100%;
  padding: 0 1rem;
  background-color: #353535;
  z-index: 2;

  @supports (backdrop-filter: blur(10px)) {
    backdrop-filter: blur(10px);
    background-color: #353535;
  }

  ${PageWrapper} {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }

  @media (min-width: 768px) {
    padding: 2rem 0;
  }
`;
