import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import 'reactjs-popup/dist/index.css';
import { GlobalStyle } from '../styles/global-styles';
import { AppModal } from './components/Modal';
import { CheckoutPage } from './pages/CheckoutPage';
import { HomePage } from './pages/HomePage/Loadable';
import { InvoicePage } from './pages/InvoicePage';
import { LoginPage } from './pages/LoginPage';
import { NotFoundPage } from './pages/NotFoundPage/Loadable';
import { RegisterPage } from './pages/RegisterPage';
import { SearchPage } from './pages/SearchPage';
import { UserProfile } from './pages/UserProfile';
import { InvoicesUserPage } from './pages/InvoicesUserPage';

import { ChangePassword } from './pages/ChangePassword';
import { AddAmount } from './pages/AddAmount';
import { AddAmountSuccess } from './pages/AddAmountSuccess';
import { ResetPassword } from './pages/ResetPassword';
import { CustomersPage } from './pages/CustomersPage';
import { UploadEmailPage } from './pages/UploadEmailPage';

export function App() {
  const { i18n } = useTranslation();
  return (
    <BrowserRouter>
      <Helmet
        titleTemplate="%s - Meo Meo IO"
        defaultTitle="Sell email gmail"
        htmlAttributes={{ lang: i18n.language }}
      >
        <meta name="description" content="Sell anumber gmail" />
      </Helmet>
      <Routes>
        <Route path={'/'} element={<HomePage />} />
        <Route path={'/checkout/:id'} element={<CheckoutPage />} />
        <Route path={'/payment/:id'} element={<InvoicePage />} />
        <Route path={'/register'} element={<RegisterPage />} />
        <Route path={'/login'} element={<LoginPage />} />
        <Route path={'/search-payment'} element={<SearchPage />} />
        <Route path={'/search-customer'} element={<CustomersPage />} />
        <Route path={'/upload-email'} element={<UploadEmailPage />} />
        <Route path={'/user-profile'} element={<UserProfile />} />
        <Route path={'/invoice/user'} element={<InvoicesUserPage />} />

        <Route path={'/change-password'} element={<ChangePassword />} />
        <Route path={'/add-amount'} element={<AddAmount />} />
        <Route path={'/naptienthanhcong/:id'} element={<AddAmountSuccess />} />
        <Route path={'/reset-password'} element={<ResetPassword />} />
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
      <GlobalStyle />
      <AppModal />
    </BrowserRouter>
  );
}
