export interface UserAmount {
  id: number;
  currency_code: string;
  user_id: number;
  total_amount: number;
  created_at: string;
  updated_at: string;
}
