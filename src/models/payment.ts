export interface Payment {
  id: number;
  payment_id: number;
  payment_status: string;
  pay_address: string;
  price_amount: number;
  price_currency: string;
  pay_amount: number;
  amount_received: number;
  pay_currency: string;
  purchase_id: number;
  created_at: string;
  updated_at: string;
  id_invoice: number;
  id_customer: number;
  id_email_type: number;
  quantity: number;
  sendMailUrlTxt: string;
  sendMailUrlXlsx: string;
}
