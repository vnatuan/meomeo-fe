export interface User {
  id: number;
  name: string;
  email: string;
  access_token: string;
  expires_in: number;
  token_type: string;
  role: number;
  created_at: string;
  updated_at: string;
}
