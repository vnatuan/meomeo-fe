export interface hoadon {
  description: string;
  quantity: BigInteger;
  price_amount: BigInteger;
  price_currency: string;
  payment_status: string;
  invoice_id: BigInteger;
  order_id: BigInteger;
  link: string;
  created_at: string;
  date_created: string;
}
