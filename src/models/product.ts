export interface ProductModel {
  id: number;
  description: string;
  ip_create: string;
  date_create: string;
  device_create: string;
  price: number;
  created_at: any;
  updated_at: any;
  has_youtube: number;
  in_stock: number;
}
