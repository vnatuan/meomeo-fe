export interface CustomerModel {
  id: number;
  name: string;
  email: string;
  total_amount: string;
  currency_code: string;
}
